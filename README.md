## Requirement

To run this project, you need to have the following:

- Docker desktop. You can install it from [Docker Docs](https://docs.docker.com/desktop/install)
- Install VS-Code extension [Dev-Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)


## Setup
After installing Docker-desktop, required extension and WSL2 (for Window).
Start up VS-code and click on the arrows face each other in the buttom
Click on *Open in Container*


## Data
Dataset used in this repo is taken from [Kaggle](https://www.kaggle.com/datasets/pigment/big-sales-data)


## Security

In this project the entire software stack is installed and executed from a Docker container.

Docker starts every container with a restricted set of linux capabilities. Capabilities turn the binary “root/non-root” dichotomy into a fine-grained access control system. For example, processes like web servers that only need to bind on a port below 1024 do not need to run as root, therefore they can just be granted the net_bind_service capability. And there are many other capabilities, one for almost all the specific areas where root privileges are usually needed. In this way it is always possible to find a correct and functional set of capabilities suitable for a specific type of container. This means that even if an intruder manages to escalate to root within a container, it is much harder to do serious damage, or to escalate to the host because his privileges would still be limited.

Furthermore the use of capabilities does not impede the use of extra layers of safety like those provided by AppArmor, SELinux, GRSEC, or other hardening systems.
